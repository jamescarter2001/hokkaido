package com.carter.hokkaido;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HokkaidoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HokkaidoApplication.class, args);
    }

}
