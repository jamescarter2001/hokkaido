package com.carter.hokkaido.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.carter.hokkaido.api.LessonXKanji;
import com.carter.hokkaido.api.response.LessonXKanjiResponse;
import com.carter.hokkaido.datasource.KanjiDataSource;
import com.carter.hokkaido.api.Kanji;
import com.carter.hokkaido.api.Lesson;
import com.carter.hokkaido.datasource.LessonDataSource;
import com.carter.hokkaido.datasource.LessonKanjiDataSource;

import java.util.List;

@RestController
@AllArgsConstructor
public class HokkaidoRestController {
    private KanjiDataSource kanjiDataSource;
    private LessonDataSource lessonDataSource;
    private LessonKanjiDataSource lessonKanjiDataSource;

    // GET
    @GetMapping("/kanji/{id}")
    public Kanji getKanjiById(@PathVariable String id) {
        return kanjiDataSource.getForId(Long.parseLong(id));
    }

    @GetMapping("/kanji")
    public List<Kanji> getKanji(@RequestParam(name = "name", required = false) String name,
                                      @RequestParam(name = "symbol", required = false) String symbol) {
        if (name != null) {
            return kanjiDataSource.getByName(name);
        } else if (symbol != null) {
            return kanjiDataSource.getByKanji(symbol.charAt(0));
        }
        return kanjiDataSource.getAll();
    }

    @GetMapping("/lesson")
    public List<LessonXKanjiResponse> getLesson(@RequestParam(name = "name", required = false) String name) {
        if (name != null) {
            return lessonKanjiDataSource.getByLessonName(name);
        }
        return null;
    }

    @GetMapping("/lesson/{id}")
    public LessonXKanjiResponse getLessonById(@PathVariable String id) {
        return lessonKanjiDataSource.getById(Long.parseLong(id));
    }

    /*@GetMapping("/lesson")
    public List<LessonXKanji> getAllLessons() {
        return lessonsXKanjiDataSource.getAll();
    }*/

    // POST
    @PostMapping("/kanji/add")
    public void addKanji(@RequestBody Kanji kanji) {
        kanjiDataSource.add(kanji);
    }

    @PostMapping("/kanji/add/multi")
    public void addMultiKanji(@RequestBody List<Kanji> kanji) {
        kanjiDataSource.add(kanji);
    }

    @PostMapping("/lesson/add")
    public void addLesson(@RequestBody Lesson lesson) {
        lessonDataSource.add(lesson);
    }

    @PostMapping("/lesson/add/multi")
    public void addMultiLesson(@RequestBody List<Lesson> lessons) {
        lessonDataSource.add(lessons);
    }

    @PostMapping("/lesson/kanji/add")
    public void addLessonKanji(@RequestBody LessonXKanji lessonXKanjis) {
        lessonKanjiDataSource.add(lessonXKanjis);
    }

    @PostMapping("/lesson/kanji/add/multi")
    public void addMultiLessonKanji(@RequestBody List<LessonXKanji> lessonXKanjis) {
        lessonKanjiDataSource.add(lessonXKanjis);
    }
}
