package com.carter.hokkaido.datasource;

import com.carter.hokkaido.api.Kanji;
import lombok.extern.slf4j.Slf4j;

import com.carter.hokkaido.entity.KanjiEntity;
import com.carter.hokkaido.repository.KanjiRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class KanjiDataSource extends DataSource<Kanji, KanjiEntity> {
    public KanjiDataSource(String name, KanjiRepository kanjiRepository, int refreshInterval) {
        super(name, kanjiRepository, refreshInterval);
    }

    @Override
    public void add(Kanji model) {
        log.info("Adding Kanji: {}", model.getSymbol());
        KanjiEntity kanjiEntity = new KanjiEntity(
                model.getId(),
                model.getSymbol(),
                model.getName(),
                model.getOnReadings(),
                model.getKunReadings(),
                model.getMeanings());

        this.save(kanjiEntity);
    }

    @Override
    protected void refresh() {
        for (KanjiEntity kanjiEntity : repository.findAll()) {
            Kanji kanji = new Kanji(
                    kanjiEntity.getId(),
                    kanjiEntity.getKanji(),
                    kanjiEntity.getName(),
                    kanjiEntity.getOnReadings(),
                    kanjiEntity.getKunReadings(),
                    kanjiEntity.getMeanings());

            this.entityMap.put(kanjiEntity.getId(), kanji);
        }
    }

    @Override
    protected void onDatabaseCreateOrUpdate(KanjiEntity kanjiEntity) {

            this.entityMap.put(kanjiEntity.getId(), new Kanji(
                    kanjiEntity.getId(),
                    kanjiEntity.getKanji(),
                    kanjiEntity.getName(),
                    kanjiEntity.getOnReadings(),
                    kanjiEntity.getKunReadings(),
                    kanjiEntity.getMeanings()));
    }

    @Override
    protected void onDatabaseDelete(KanjiEntity kanjiEntity) {
        this.entityMap.remove(kanjiEntity.getId());
    }

    public List<Kanji> getByName(String name) {
        List<Kanji> kanjiList = new ArrayList<>();

        String searchName = name.toUpperCase();

        for (Kanji kanji : this.entityMap.values()) {
            String entityName = kanji.getName().toUpperCase();

            if (entityName.contains(searchName)) {
                kanjiList.add(kanji);
            }
        }
        return kanjiList;
    }

    public List<Kanji> getByKanji(char symbol) {
        for (Kanji kanji : this.entityMap.values()) {
            if (kanji.getSymbol() == symbol) {
                return Collections.singletonList(kanji);
            }
        }
        return null;
    }
}