package com.carter.hokkaido.datasource;

import lombok.extern.slf4j.Slf4j;

import com.carter.hokkaido.api.Kanji;
import com.carter.hokkaido.api.Lesson;
import com.carter.hokkaido.api.response.LessonXKanjiResponse;
import com.carter.hokkaido.entity.LessonsXKanjiEntity;
import com.carter.hokkaido.api.LessonXKanji;
import com.carter.hokkaido.repository.LessonsXKanjiRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public class LessonKanjiDataSource extends DataSource<LessonXKanji, LessonsXKanjiEntity> {
    private final KanjiDataSource kanjiDataSource;
    private final LessonDataSource lessonDataSource;

    public LessonKanjiDataSource(String name,
                                 LessonsXKanjiRepository lessonsXKanjiRepository,
                                 KanjiDataSource kanjiDataSource,
                                 LessonDataSource lessonDataSource,
                                 int refreshInterval) {

        super(name, lessonsXKanjiRepository, refreshInterval);

        this.kanjiDataSource = kanjiDataSource;
        this.lessonDataSource = lessonDataSource;
    }


    @Override
    public void add(LessonXKanji lessonXKanji) {
        Optional<Kanji> kanji = Optional.ofNullable(kanjiDataSource.getForId(lessonXKanji.getKanjiId()));
        Optional<Lesson> lesson = Optional.ofNullable(lessonDataSource.getForId(lessonXKanji.getLessonId()));

        if (kanji.isPresent() && lesson.isPresent()) {
            Kanji resolvedKanji = kanji.get();
            Lesson resolvedLesson = lesson.get();

            log.info("Adding Kanji {} to Lesson: {}", resolvedKanji.getSymbol(), resolvedLesson.getName());
            LessonsXKanjiEntity lessonsXKanjiEntity = new LessonsXKanjiEntity(
                    lessonXKanji.getLessonId(),
                    lessonXKanji.getKanjiId());

            this.save(lessonsXKanjiEntity);
        } else {
            log.error("Error adding Kanji to Lesson {}: {}", lessonXKanji.getLessonId(), lessonXKanji.getKanjiId());
        }
    }

    @Override
    protected void refresh() {
        for (LessonsXKanjiEntity lessonsXKanjiEntity : repository.findAll()) {
            LessonXKanji lessonXKanji = new LessonXKanji(
                    lessonsXKanjiEntity.getId(),
                    lessonsXKanjiEntity.getLessonId(),
                    lessonsXKanjiEntity.getKanjiId());

            this.entityMap.put(lessonXKanji.getId(), lessonXKanji);
        }
    }

    @Override
    protected void onDatabaseCreateOrUpdate(LessonsXKanjiEntity lessonsXKanjiEntity) {
        this.entityMap.put(lessonsXKanjiEntity.getId(), new LessonXKanji(
                lessonsXKanjiEntity.getId(),
                lessonsXKanjiEntity.getLessonId(),
                lessonsXKanjiEntity.getKanjiId()));
    }

    @Override
    protected void onDatabaseDelete(LessonsXKanjiEntity lessonsXKanjiEntity) {
        this.entityMap.remove(lessonsXKanjiEntity.getId());
    }

    public List<LessonXKanjiResponse> getByLessonName(String name) {
        List<Lesson> lessons = lessonDataSource.getByName(name);
        return this.getForLessons(lessons);
    }

    public LessonXKanjiResponse getById(Long id) {
        Lesson lesson = lessonDataSource.getForId(id);
        return new LessonXKanjiResponse(lesson, this.getKanjiForLesson(lesson));
    }

    private List<Kanji> getKanjiForLesson(Lesson lesson) {
        List<Kanji> kanji = new ArrayList<>();

        for (LessonXKanji lessonXKanji : this.entityMap.values()) {
            if (lessonXKanji.getLessonId().equals(lesson.getId())) {
                kanji.add(kanjiDataSource.getForId(lessonXKanji.getKanjiId()));
            }
        }
        return kanji;
    }

    private List<LessonXKanjiResponse> getForLessons(List<Lesson> lessons) {
        List<LessonXKanjiResponse> lessonXKanjiResponseList = new ArrayList<>();

        for (Lesson lesson : lessons) {
            List<Kanji> kanji = getKanjiForLesson(lesson);

            if (kanji.size() != 0) {
                LessonXKanjiResponse response = new LessonXKanjiResponse(lesson, kanji);
                lessonXKanjiResponseList.add(response);
            }
        }
        return lessonXKanjiResponseList;
    }
}