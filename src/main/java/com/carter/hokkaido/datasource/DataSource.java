package com.carter.hokkaido.datasource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Slf4j
public abstract class DataSource<T, E> {
    protected final String name;
    protected final CrudRepository<E, Long> repository;
    protected final Map<Long, T> entityMap;

    private final ExecutorService executorService;

    public DataSource(String name, CrudRepository<E, Long> repository, int refreshInterval) {
        this.name = name;
        this.repository = repository;
        this.entityMap = new ConcurrentHashMap<>();
        this.executorService = Executors.newSingleThreadExecutor();

        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(this::refreshFromDatabase,0, refreshInterval, TimeUnit.MINUTES);
    }

    abstract public void add(T model);

    abstract protected void refresh();
    abstract protected void onDatabaseCreateOrUpdate(E entity);
    abstract protected void onDatabaseDelete(E entity);

    private void refreshFromDatabase() {
        log.info("Refreshing from database for Data Source: {}", this.name);
        this.refresh();
        log.info("Refresh complete for Data Source: {}", this.name);
    }

    public void add(List<T> models) {
        for (T model : models) {
            this.add(model);
        }
    }

    public List<T> getAll() {
        return new ArrayList<>(this.entityMap.values());
    }

    public T getForId(Long id) {
        return this.entityMap.get(id);
    }

    public void save(E entity) {
        executorService.execute(() -> this.addToDatabase(entity));
    }

    public void delete(E entity) {
        executorService.execute(() -> this.removeFromDatabase(entity));
    }

    private void addToDatabase(E entity) {
        this.repository.save(entity);
        this.onDatabaseCreateOrUpdate(entity);
    }

    private void removeFromDatabase(E entity) {
        this.repository.delete(entity);
        this.onDatabaseDelete(entity);
    }
}
