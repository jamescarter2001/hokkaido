package com.carter.hokkaido.datasource;

import com.carter.hokkaido.entity.LessonEntity;
import com.carter.hokkaido.api.Lesson;
import com.carter.hokkaido.repository.LessonRepository;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class LessonDataSource extends DataSource<Lesson, LessonEntity> {
    public LessonDataSource(String name, LessonRepository lessonRepository, int refreshInterval) {
        super(name, lessonRepository, refreshInterval);
    }


    @Override
    public void add(Lesson lesson) {
        log.info("Adding Lesson: {}", lesson.getName());
        LessonEntity lessonEntity = new LessonEntity(
                lesson.getId(),
                lesson.getName(),
                lesson.getIsDefault());

        this.save(lessonEntity);
    }

    @Override
    protected void refresh() {
        for (LessonEntity lessonEntity : repository.findAll()) {
            Lesson lesson = new Lesson(
                    lessonEntity.getId(),
                    lessonEntity.getName(),
                    lessonEntity.getIsDefault());

            this.entityMap.put(lessonEntity.getId(), lesson);
        }
    }

    @Override
    protected void onDatabaseCreateOrUpdate(LessonEntity lessonEntity) {
        this.entityMap.put(lessonEntity.getId(), new Lesson(
                lessonEntity.getId(),
                lessonEntity.getName(),
                lessonEntity.getIsDefault()));
    }

    @Override
    protected void onDatabaseDelete(LessonEntity lessonEntity) {
        this.entityMap.remove(lessonEntity.getId());
    }

    public void remove(Long id) {
        Lesson lesson = this.entityMap.get(id);

        if (lesson != null) {
            log.info("Removing Lesson: {}", lesson.getName());
            this.entityMap.remove(id);

            LessonEntity lessonEntity = new LessonEntity(
                    lesson.getId(),
                    lesson.getName(),
                    lesson.getIsDefault());

            this.delete(lessonEntity);
        }
    }

    public List<Lesson> getByName(String name) {
        List<Lesson> lessonList = new ArrayList<>();

        String upperCaseName = name.toUpperCase();
        for (Lesson lesson : entityMap.values()) {
            if (lesson.getName().toUpperCase().equals(upperCaseName)) {
                lessonList.add(lesson);
            }
        }
        return lessonList;
    }
}