package com.carter.hokkaido.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hkd_lessons_x_kanji")
@Data
@NoArgsConstructor
public class LessonsXKanjiEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    Long lessonId;
    Long kanjiId;

    public LessonsXKanjiEntity(Long lessonId, Long kanjiId) {
        this.lessonId = lessonId;
        this.kanjiId = kanjiId;
    }
}
