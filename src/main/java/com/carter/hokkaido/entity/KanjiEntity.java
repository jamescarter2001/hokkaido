package com.carter.hokkaido.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.carter.hokkaido.converter.StringListConverter;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "hkd_kanji")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class KanjiEntity {
    @Id
    private Long id;
    private char kanji;
    private String name;
    @Convert(converter = StringListConverter.class)
    private List<String> onReadings;
    @Convert(converter = StringListConverter.class)
    private List<String> kunReadings;
    @Convert(converter = StringListConverter.class)
    private List<String> meanings;
}