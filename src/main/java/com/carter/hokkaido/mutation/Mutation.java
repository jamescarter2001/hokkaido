package com.carter.hokkaido.mutation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

enum MutationType {
    ADD, REMOVE
}

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Mutation {
    MutationType mutationType;
}
