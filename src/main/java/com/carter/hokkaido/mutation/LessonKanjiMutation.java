package com.carter.hokkaido.mutation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonKanjiMutation extends Mutation {
    Long lessonId;
    List<Long> kanjiIds;
}
