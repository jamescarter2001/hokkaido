package com.carter.hokkaido.config;

import com.carter.hokkaido.datasource.KanjiDataSource;
import com.carter.hokkaido.datasource.LessonDataSource;
import com.carter.hokkaido.datasource.LessonKanjiDataSource;
import com.carter.hokkaido.repository.KanjiRepository;
import com.carter.hokkaido.repository.LessonRepository;
import com.carter.hokkaido.repository.LessonsXKanjiRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {
    @Value( "${database.refresh.interval}" )
    private int refreshInterval;

    @Bean
    public KanjiDataSource kanjiDataSource(KanjiRepository kanjiRepository) {
        return new KanjiDataSource("KanjiDataSource", kanjiRepository, refreshInterval);
    }

    @Bean
    public LessonDataSource lessonDataSource(LessonRepository lessonRepository) {
        return new LessonDataSource("LessonDataSource", lessonRepository, refreshInterval);
    }

    @Bean
    public LessonKanjiDataSource lessonKanjiDataSource(LessonsXKanjiRepository lessonsXKanjiRepository,
                                                       KanjiDataSource kanjiDataSource,
                                                       LessonDataSource lessonDataSource) {
        return new LessonKanjiDataSource("LessonKanjiDataSource", lessonsXKanjiRepository, kanjiDataSource, lessonDataSource, refreshInterval);
    }
}
