package com.carter.hokkaido.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Lesson extends Model {
    private Long id;
    private String name;
    private Boolean isDefault;
}
