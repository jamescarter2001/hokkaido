package com.carter.hokkaido.api.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import com.carter.hokkaido.api.Kanji;
import com.carter.hokkaido.api.Lesson;

import java.util.List;

@Data
@AllArgsConstructor
public class LessonXKanjiResponse {
    private Lesson lesson;
    private List<Kanji> kanji;
}
