package com.carter.hokkaido.api;

import lombok.Data;

@Data
public class Model {
    protected Long id;
}
