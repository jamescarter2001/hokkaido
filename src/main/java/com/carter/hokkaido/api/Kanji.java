package com.carter.hokkaido.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class Kanji extends Model {
    private char symbol;
    private String name;
    private List<String> onReadings;
    private List<String> kunReadings;
    private List<String> meanings;

    public Kanji(Long id, char symbol, String name, List<String> onReadings,
                 List<String> kunReadings, List<String> meanings) {
        this.id = id;
        this.symbol = symbol;
        this.name = name;
        this.onReadings = onReadings;
        this.kunReadings = kunReadings;
        this.meanings = meanings;
    }
}
