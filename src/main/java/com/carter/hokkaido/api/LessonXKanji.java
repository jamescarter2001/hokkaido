package com.carter.hokkaido.api;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LessonXKanji extends Model {
    private Long lessonId;
    private Long kanjiId;

    public LessonXKanji(Long id, Long lessonId, Long kanjiId) {
        this.id = id;
        this.lessonId = lessonId;
        this.kanjiId = kanjiId;
    }

    public LessonXKanji(Long lessonId, Long kanjiId) {
        this.lessonId = lessonId;
        this.kanjiId = kanjiId;
    }
}
