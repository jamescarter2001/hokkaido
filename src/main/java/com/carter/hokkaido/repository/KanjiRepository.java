package com.carter.hokkaido.repository;

import com.carter.hokkaido.entity.KanjiEntity;
import org.springframework.data.repository.CrudRepository;

public interface KanjiRepository extends CrudRepository<KanjiEntity, Long> { }
