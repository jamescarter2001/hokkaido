package com.carter.hokkaido.repository;

import com.carter.hokkaido.entity.LessonEntity;
import org.springframework.data.repository.CrudRepository;

public interface LessonRepository extends CrudRepository<LessonEntity, Long> {
}
