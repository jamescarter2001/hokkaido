package com.carter.hokkaido.repository;

import com.carter.hokkaido.entity.LessonsXKanjiEntity;
import org.springframework.data.repository.CrudRepository;

public interface LessonsXKanjiRepository extends CrudRepository<LessonsXKanjiEntity, Long> {}
