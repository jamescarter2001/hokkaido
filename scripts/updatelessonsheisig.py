import requests
import json

class LessonData:
    def __init__(self, lower, upper):
        self.lower = lower
        self.upper = upper

    def get_lesson_kanji(self):
        lessons = []
        for i in range(self.lower, self.upper+1):
            lessons.append(i)
        
        return lessons

def obj_dict(obj):
    return obj.__dict__

lessons = [
    LessonData(1, 15),
    LessonData(16, 34),
    LessonData(35, 54),
    LessonData(55, 74),
    LessonData(75, 98),
    LessonData(99, 109),
    LessonData(110, 133),
    LessonData(134, 184),
    LessonData(185, 206),
    LessonData(207, 249),
    LessonData(250, 264),
    LessonData(265, 294),
    LessonData(295, 320),
    LessonData(321, 345),
    LessonData(346, 376),
    LessonData(377, 395),
    LessonData(396, 422),
    LessonData(423, 514),
    LessonData(515, 547),
    LessonData(548, 553),
    LessonData(554, 619),
    LessonData(620, 686),
    LessonData(687, 828),
    LessonData(829, 858),
    LessonData(859, 957),
    LessonData(958, 1022),
    LessonData(1023, 1103),
    LessonData(1104, 1123),
    LessonData(1124, 1166),
    LessonData(1167, 1205),
    LessonData(1206, 1267),
    LessonData(1268, 1304),
    LessonData(1305, 1336),
    LessonData(1337, 1389),
    LessonData(1390, 1430),
    LessonData(1431, 1496),
    LessonData(1497, 1533),
    LessonData(1534, 1595),
    LessonData(1596, 1650),
    LessonData(1651, 1710),
    LessonData(1711, 1742),
    LessonData(1743, 1776),
    LessonData(1777, 1812),
    LessonData(1813, 1845),
    LessonData(1846, 1893),
    LessonData(1894, 1913),
    LessonData(1914, 1945),
    LessonData(1946, 1969),
    LessonData(1970, 1996),
    LessonData(1997, 2024),
    LessonData(2025, 2052),
    LessonData(2053, 2076),
    LessonData(2077, 2131),
    LessonData(2132, 2161),
    LessonData(2162, 2181),
    LessonData(2182, 2200),
]

class LessonKanji:
    def __init__(self, lessonId, kanjiId):
        self.lessonId = lessonId
        self.kanjiId = kanjiId

lesson_num = 56
lesson_kanji_array = []

for lesson in lessons:
    lesson_num += 1
    for k in lesson.get_lesson_kanji():
        lesson_kanji = LessonKanji(lesson_num, k)
        lesson_kanji_array.append(lesson_kanji)

request_headers = {
    "Content-Type": "application/json; charset=utf-8"
}

response = requests.post("http://localhost:8080/lesson/kanji/add/multi", json.dumps(lesson_kanji_array, default=obj_dict), headers=request_headers)
if response.status_code != 200:
    print(f"Error: {response.status_code}")
print("Done.")