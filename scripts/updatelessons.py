import requests
import json

class LessonEntity: 
    def __init__(self, name, is_default):
        self.name = name
        self.isDefault = is_default

def obj_dict(obj):
    return obj.__dict__

heisig_lessons = []
heisig_lesson_count = 56

for i in range(1, heisig_lesson_count+1):
    entity : LessonEntity = LessonEntity(f"Lesson {i}", True)
    heisig_lessons.append(entity)

heisig_lessons_json = json.dumps(heisig_lessons, default=obj_dict)

request_headers = {
    "Content-Type": "application/json; charset=utf-8"
}

response = requests.post("http://localhost:8080/lesson/add/multi", heisig_lessons_json, headers=request_headers)
if response.status_code != 200:
    print(f"Error: {response.status_code}")
print("Done.")