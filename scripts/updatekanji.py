import requests
import json
import csv

class HeisigKanji:
    def __init__(self, kanji, id_5th_ed, id_6th_ed):
        self.kanji = kanji
        self.id_5th_ed = id_5th_ed
        self.id_6th_ed = id_6th_ed

class Kanji:
    def __init__(self, name, kanji, on_readings, kun_readings, examples, meanings):
        self.name = name
        self.kanji = kanji
        self.on_readings = on_readings
        self.kun_readings = kun_readings
        self.examples = examples
        self.meanings = meanings

class KanjiEntity:
    def __init__(self, id, name, kanji, on_readings, kun_readings, meanings):
        self.id = id
        self.name = name
        self.symbol = kanji
        self.onReadings = on_readings
        self.kunReadings = kun_readings
        self.meanings = meanings

def clean(kanji : Kanji):
    kanji.on_readings = list(filter(lambda x: x != "", kanji.on_readings))
    kanji.kun_readings = list(filter(lambda x: x != "", kanji.kun_readings))
    kanji.examples = list(filter(lambda x: x != "", kanji.examples))
    kanji.meanings = list(filter(lambda x: x != "", kanji.meanings))

    return kanji

def obj_dict(obj):
    return obj.__dict__

kanji_data = []
heisig_kanji = {}

f = open('res/kanjidata.json', encoding='utf-8')

json_data = json.load(f)
bad_data = ['']

for node in json_data:
    kanji : Kanji = Kanji(node[0], node[1], node[2], node[3], node[4], node[5])
    kanji_data.append(clean(kanji))

test = [x for x in kanji_data if x.kanji == "魔"][0]

heisig_csv = open('res/heisig-kanjis.csv', encoding='utf-8')
spamreader = csv.reader(heisig_csv, delimiter=',', quotechar='|')

csv_line = 0
for row in spamreader:
    if csv_line == 0:
        csv_line += 1
        continue
    if row[2].isdigit():
        heisig_kanji[row[0]] = int(row[2])

kanji_entities = list(map(lambda x: KanjiEntity(heisig_kanji[x.kanji], x.name, x.kanji, x.on_readings, x.kun_readings, x.meanings), kanji_data))
kanji_entities.sort(key=lambda x: x.id)

kanji_entities_json = json.dumps(kanji_entities, default=obj_dict)

request_headers = {
    "Content-Type": "application/json; charset=utf-8"
}

response = requests.post("http://localhost:8080/kanji/add/multi", kanji_entities_json, headers=request_headers)
if response.status_code != 200:
    print(f"Error: {response.status_code}")
print("Done.")